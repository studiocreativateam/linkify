Installation
------------

`composer require studiocreativateam/linkify`

Usage
-----

```php
$linkify = new \SCTeam\Linkify();
$text = 'This is my text containing a link to www.example.com.';

echo $linkify->process($text);
```

Will output:

```html
This is my text containing a link to <a href="http://www.example.com">www.example.com</a>.
```

### Options

Options set on the constructor will be applied to all links. Alternatively you can place the options on a method call. The latter will override the former.

```php
$linkify = new \SCTeam\Linkify(['attr' => ['class' => 'foo']]);
$text = 'This is my text containing a link to www.example.com.';

echo $linkify->process($text);
```

Will output:

```html
This is my text containing a link to <a href="http://www.example.com" class="foo">www.example.com</a>.
```

Whereas:

```php
$linkify = new \SCTeam\Linkify(['attr' => ['class' => 'foo']]);
$text = 'This is my text containing a link to www.example.com.';

echo $linkify->process($text, ['attr' => ['class' => 'bar']]);
```

Will output:

```html
This is my text containing a link to <a href="http://www.example.com" class="bar">www.example.com</a>.
```

Available options are:

#### `attr`

An associative array of HTML attributes to add to the link. For example:

```php
['attr' => ['class' => 'foo', 'style' => 'font-weight: bold; color: red;']]
```

#### `callback`

A closure to call with each url match. The closure will be called for each URL found with three parameters: the url, the caption and a boolean `isEmail` (if `$isEmail` is true, then `$url` is equals to `$caption`.

If the callback return a non-null value, this value replace the link in the resulting text. If null is returned, the usual `<a href="URL">CAPTION</a>` is used.

```php
$linkify = new \SCTeam\Linkify(['callback' => fn($url, $caption, $isEmail) => '<b>' . $caption . '</b>']);
echo $linkify->process('This link will be converted to bold: www.example.com.'));
```
